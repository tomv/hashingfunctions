var seeds = [3, 6, 9];

var hashingFunction1 = function (range, value) {
    genericHash(range, value, seeds[0]);
}

var hashingFunction2 = function (range, value) {
    genericHash(range, value, seeds[1]);

}

var hashingFunction3 = function (range, value) {
    genericHash(range, value, seeds[2]);

}

var hashingFunctions = [hashingFunction1, hashingFunction2, hashingFunction3];

function genericHash(range, value, seed) {
    var chars = value.split('');

    var result = 0;
    chars.forEach(function (val, index, arr) {
        result += val.charCodeAt(0);
    });
    result += seed;
    var final_hash = result % range;
    console.log(final_hash)
    return (final_hash);

}
